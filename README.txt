This module allows you to set a thumbnail preview for any content type.

HOW TO USE IT
======================
1. Install and enable the module
2. Go to your Content Types, under the admin/structure page
3. Click edit on the content type you want to add an image for
4. Under "Thumbnail" you'll find an image field, upload your image
5. Go to Add Content, you'll see your new thumbnail next to your content type

SETTINGS
======================
1. All thumbnails use the Image Style 'content_type_thumbnail', you can change
the settings of this style to suit your needs here:

  admin/config/media/image-styles

2. There are two layouts options for the node add screen. Either grid or list.
You can change this setting here:

  admin/config/content/content_type_thumbnail

NOTES
======================
Make sure your files directory has the correct permissions as required
by Drupal


AUTHOR/MAINTAINER
======================
Mike Spence (mikespence) - http://drupal.org/user/2007822
